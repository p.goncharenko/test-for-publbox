# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# test credentials
service = 'https://app.publbox.com/authorization/signin'

user = 'goncharenkopaul@gmail.com'
password = '12345678'

source_feed = 'http://feeds.bbci.co.uk/news/world/rss.xml'
link_feed = 'https://app.publbox.com/main/feed'

# initialization browser
driver = webdriver.Chrome()
driver.maximize_window()

# login
driver.get(service)
email_input = driver.find_element_by_name('email')
email_input.send_keys(user)
pwd_input = driver.find_element_by_name('password')
pwd_input.send_keys(password)
driver.find_element_by_xpath("//button[@type='submit']").click()

# open RSS-add menu
driver.implicitly_wait(3)
driver.find_element_by_css_selector("a[href$='/main/feed']").click()

# add new RSS
driver.switch_to.frame(driver.find_element_by_tag_name("iframe"))
driver.switch_to.frame(driver.find_element_by_class_name("rssFrame"))
driver.find_element_by_css_selector("#app button").click()
driver.implicitly_wait(3)
link_input = driver.find_element_by_name("rssLink")
link_input.send_keys(source_feed)
link_input.send_keys(Keys.ENTER)

# checking add feed
driver.implicitly_wait(3)
new_feed = driver.find_element_by_partial_link_text("www.bbc.co.uk/news")
x = new_feed.get_attribute("href")
assert x == 'https://www.bbc.co.uk/news/', "Link incorrect"

# checking content on feed list
driver.get(link_feed)
driver.switch_to.frame(driver.find_element_by_tag_name("iframe"))
driver.switch_to.frame(driver.find_element_by_class_name("rssFrame"))
item_feed = driver.find_elements_by_xpath(
    "//div[contains(@class, 'FeedPostItem__FeedLink')]")

if len(item_feed) == 0:
    print('Test failed')
else:
    print('Test successful')

# end of test
driver.quit()
