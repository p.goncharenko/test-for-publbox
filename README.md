Conditions

Create user, use email + password https://app.publbox.com/authorization/signup (do not automize this step, make it manual and use credentials at test)
1. Sign in as test user
2. Open to RSS Integrator, use left side menu (https://app.publbox.com/main/feed)
3. Add feed http://feeds.bbci.co.uk/news/world/rss.xml
4. Check that related topics appears at feed

The code
Minimum working version for verification

For questions, please contact Skype: Paulll888

To test the code you need:
- Selenium
- Web driver
- Browser Driver (Chrome)
- Python 3

Launch command:
python tests/test_add_feed.py

Note
After starting and successfully running the test, information that was created in the process is not deleted.
This means that re-running this test does not make sense. By the way, this does not contradict the established conditions.